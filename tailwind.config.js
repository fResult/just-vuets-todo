module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: '#42B983',
        secondary: '#B98342'
      }
    }
  },
  variants: {
    extend: {}
  },
  plugins: []
}
