import { createRouter, createWebHistory } from 'vue-router'
import PageHome from './pages/PageHome.vue'
import PageTodos from './pages/PageTodos.vue'
import PageNotFound from './pages/PageNotFound.vue'

const router = createRouter({
  end: false,
  history: createWebHistory(),
  sensitive: false,
  strict: false,
  routes: [
    { path: '/', name: 'PageHome', component: PageHome, strict: true },
    { path: '/todos', name: 'PageTodos', component: PageTodos, strict: true },
    {
      path: '/:catchAll(.*)*',
      name: 'PageNotFound',
      component: PageNotFound
    }
  ]
})

export default router
