declare module '*.vue' {
  import Vue, { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component
}
